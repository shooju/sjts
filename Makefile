PROGRAM=libsjts.a
CPP=gcc
LIBS=-lm
SOURCE=.
OBJS=sjtsenc.o sjtsdec.o ultrajsonenc.o ultrajsondec.o
LINKFLAGS=-shared 
#-Wl,-soname,libsjts.so.1

CPPFLAGS=-D_REENTRANT -D_LINUX
all : CPPFLAGS += -O3 -DNDEBUG -fPIC
all : libsjts

libsjts : $(OBJS)
	ar rcs ./$(PROGRAM) $(OBJS)

%.o:    $(SOURCE)/%.c
	$(CPP) $(CPPFLAGS) -c $< -o ./$@

clean:
	rm -rf *.o
	rm -rf $(PROGRAM)
